var autoprefixer = require('gulp-autoprefixer'),
browserSync = require('browser-sync').create(),
cleanCSS = require('gulp-clean-css'),
gulp = require('gulp'),
rename = require('gulp-rename'),
sass = require('gulp-sass'),
sourcemaps = require('gulp-sourcemaps'),
ts = require('gulp-typescript'),
uglify = require('gulp-uglify');

gulp.task('html', function() {
	gulp.src('*.html');
});

gulp.task('sass', function() {
	return gulp.src('src/scss/styles.scss')
		.pipe(sourcemaps.init())
			.pipe(sass().on('error', sass.logError))
			.pipe(autoprefixer())
			.pipe(cleanCSS({compatibility: '*'})) // ie10+
			.pipe(rename({suffix: ".min"}))
		.pipe(sourcemaps.write('/'))
		.pipe(gulp.dest('assets/css'))
});

gulp.task('ts', function() {
	return gulp.src('src/ts/main.ts')
      .pipe(sourcemaps.init())
         .pipe(ts({outfile: 'main.js'}))
			.pipe(uglify())
			.pipe(rename({suffix: '.min'}))
		.pipe(sourcemaps.write('/'))
		.pipe(gulp.dest('assets/js'))
});

gulp.task('watch', function() {
	gulp.watch('src/scss/**/*.scss', ['sass']);
	gulp.watch('src/ts/**/*.ts', ['ts']);
});

gulp.task('serve', function() {
   browserSync.init({
		server: {
			baseDir: "./",
			index: "index.html"
		},
		open: true
  });
	gulp.watch('*.html', gulp.series('html')).on('change', function() { browserSync.reload(); });
	gulp.watch('src/scss/**/*.scss', gulp.series('sass')).on('change', function() { browserSync.reload(); });
	gulp.watch('src/ts/**/*.ts', gulp.series('ts')).on('change', function() { browserSync.reload(); });
});